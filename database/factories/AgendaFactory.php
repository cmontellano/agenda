<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AgendaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombres' => $this->faker->name(),
            'apellidos' => $this->faker->lastName,
            'celular' => $this->faker->numberBetween(11111111, 99999999),
            'email' => $this->faker->email,
            'direccion' => $this->faker->address,

        ];
    }
}
