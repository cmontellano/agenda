<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listar</title>
</head>
<body>
    
    <table>
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Celular</th>
            <th>Email</th>
            <th>Direccion</th>
            <th>Acciones</th>
        </tr>
        @foreach($agendas as $agenda)
        <tr>
            <td>{{$agenda->nombres}}</td>
            <td>{{$agenda->apellidos}}</td>
            <td>{{$agenda->celular}}</td>
            <td>{{$agenda->email}}</td>
            <td>{{$agenda->direccion}}</td>
            <td>
                <a href="{{route('agendas.edit', $agenda->id)}}">Editar</a>
                <a href="{{route('agendas.show', $agenda->id)}}">Mostrar</a>
                <form action="{{route('agendas.destroy', $agenda->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Eliminar">
                </form>
                
        </tr>
        @endforeach
    </table>
    <a href="{{route('agendas.create')}}">Insertar</a>
    
</body>
</html>