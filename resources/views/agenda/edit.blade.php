<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listar</title>
</head>
<body>
    <form action="{{route('agendas.update',$agenda->id)}}" method="POST">
        @csrf
        @method('PUT')
        <input type="text" name="nombres" placeholder="Nombres" value="{{$agenda->nombres}}"><br>
        <input type="text" name="apellidos" placeholder="Apellidos" value="{{$agenda->apellidos}}"><br>
        <input type="number" name="celular" placeholder="Celular" value="{{$agenda->celular}}"><br>
        <input type="email" name="email" placeholder="Email" value="{{$agenda->email}}"><br>
        <input type="text" name="direccion" placeholder="Direccion" value="{{$agenda->direccion}}"><br>
        <input type="submit" value="Editar">

    </form>
    
</body>
</html>