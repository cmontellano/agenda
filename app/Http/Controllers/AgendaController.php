<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agendas = Agenda::get();
        
        return view('agenda.index', 
            [
                'agendas' => $agendas
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agenda = new Agenda();
        $agenda->nombres = $request->nombres;
        $agenda->apellidos = $request->apellidos;
        $agenda->celular = $request->celular;
        $agenda->email = $request->email;
        $agenda->direccion = $request->direccion;
        $agenda->save();
        
        return redirect()->route('agendas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function show(Agenda $agenda)
    {
        return view('agenda.show', 
            [
                'agenda' => $agenda
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit(Agenda $agenda)
    {
        return view('agenda.edit', 
            [
                'agenda' => $agenda
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agenda $agenda)
    {
        $agenda->nombres = $request->nombres;
        $agenda->apellidos = $request->apellidos;
        $agenda->celular = $request->celular;
        $agenda->email = $request->email;
        $agenda->direccion = $request->direccion;
        $agenda->save();
        
        return redirect()->route('agendas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agenda $agenda)
    {
        $agenda->delete();
        
        return redirect()->route('agendas.index');
    }
}
